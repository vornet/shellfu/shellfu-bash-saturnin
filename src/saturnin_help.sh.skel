#!/bin/bash

shellfu import exit
shellfu import inigrep
shellfu import pretty

#
# Saturnin help subsystem
#
# This module implements simple help subsystem for Saturnin.
#


#          #                             topic discussed below this line #
# INTERNAL # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#          #                        is "don't, just DON'T use this code" #

_saturnin_help__main() {
    #
    # Show help to user
    #
    # This should not be called directly but from saturnin__main via
    # builtin mechanism.
    #
    local topic         # selected topic, if any
    local AllTopics     # all known topics
    local TopicPath     # path to look for topic files
    local TopicSuffix=.md   # path suffix to recognize topic files
    local action=print  # action to take
    while true; do case $1 in
        --topicpath) TopicPath=$2
            shift 2 || __saturnin_help__usage -w "no --topicpath" ;;
        --topicsuffix) TopicSuffix=$2
            shift 2 || __saturnin_help__usage -w "no --topicsuffix" ;;
        -a|--all) __saturnin_help__print_all; exit ;;
        -*)  __saturnin_help__usage -w "unknown option: $1" ;;
        /)   __saturnin_help__usage -w "missing REGEX: $1"  ;;
        /*)  action=search; break ;;
        *)   break ;;
    esac done
    mapfile -t AllTopics <<<"$(__saturnin_help__lstopics)"
    test ${#AllTopics[@]} -gt 0 \
     || saturnin__bug "no topics found: $TopicPath"
    topic=$1
    debug -v AllTopics topic
    case $action in
        search) __saturnin_help__search "${1#/}"; exit ;;
        print)  ;;
    esac
    test -n "$topic" \
     || __saturnin_help__usage -w "no TOPIC?"
    __saturnin_help__lstopics | grep -qxFe "$topic" \
     || __saturnin_help__usage -w "no such topic: $topic"
    __saturnin_help__print "$topic"; return 0
}

_saturnin_help__compgen() {
    #
    # Handle tab completion
    #
    local TopicPath=$1
    local TopicSuffix=$2
    local pos=$3
    local previous=$4
    case $pos:$previous in
        0:help)
            __saturnin_help__lstopics
            echo -a
            echo --all
            ;;
    esac
}

__saturnin_help__usage() {
    #
    # Print usage message and exit
    #
    PRETTY_USAGE="self=${0##*/} help" \
    mkusage "$@" \
            "TOPIC" \
            "-a | --all     # to display all topics" \
            "/REGEX         # to display all topics that match REGEX" \
        -- \
        "topics:" \
        -i \
            "${AllTopics[@]}"
}

__saturnin_help__lstopics() {
    #
    # List all topics found
    #
    find "$TopicPath" \
        -type f \
        -name "*$TopicSuffix" \
      | sed "
            s:^$TopicPath/*::
            s:$TopicSuffix$::
        "
}

__saturnin_help__print() {
    #
    # Print topic $1
    #
    local tname=$1      # topic name
    local tfile         # topic file path
    tfile=$TopicPath/$tname$TopicSuffix
    test -n "$tfile" || {
        warn "no such topic: $tname"
        return 3
    }
    cat "$tfile"
}

__saturnin_help__search() {
    #
    # Search in topics using RE $1 and display that
    #
    local re=$1     # provided RE
    local ges       # each grep's exit status
    local es=1      # exit status of this function
    local topic     # each known topic
    debug -v re
    for topic in "${AllTopics[@]}"; do
        debug -v topic
        __saturnin_help__print "$topic" \
          | grep --color=always -C 1000000 -i -e "$re"; ges=$?
        test $ges -ge 2 && return $ges
        test $ges -eq 0 && es=0
    done
    test $es -gt 0 && warn "no match: $re"
    return $es
}

__saturnin_help__print_all() {
    #
    # Print all topics
    #
    local topic     # each known topic
    for topic in $(__saturnin_help__lstopics); do
        __saturnin_help__print "$topic"
    done
}

#shellfu module-version=__MKIT_PROJ_VERSION__
